<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() ;
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente'])	)
			{
				echo "Hi, ";
				print $_SESSION['username'];
				if(isset($_SESSION['adm']) AND $_SESSION['adm']==md5($_SESSION['username']))
					print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				else
					print "|</span> <span class=\"myAccount\"><a href=\"my_account.php\">My Account</a></span> <a href=\"logout.php\">Logout</a></h3>";
			}	
		   else
			    print "</span> <span class=\"myAccount\"><a href=\"sign_up.php\">Sign Up</a> | </span> <a href=\"login_page.php\">Login</a></h3>"; 
				
	?>

	
        
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="admin_page.php" >Admin area</a></li>
		<li><a href="my_account.php" >My Account</a></li>
      </ul>
    </div>

    <div class="checkout grid_16">
      

	</br>
	<!-- INSERIMENTO PIATTO -->
      <div class="loginPage grid_8 omega">
        <h4>Conferma ordine</h4>
        <form action="order_confirmation_2.php" method="post">
          <fieldset>
			
			<?php
			$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw); 

			$query = "SELECT orario_desiderato FROM ordine WHERE id_ordine = (SELECT MAX(id_ordine) FROM ordine)";
			$result = pg_query($query);
			$row = pg_fetch_assoc($result);
			$tempo_richiesto= $row['orario_desiderato'];
			?>
			
			
			
           Il tuo ordine sarà pronto alle:
			<?php		   
			
			$utente = $_SESSION['id_utente'];
			$query = "SELECT (cast(date_trunc('minute', ((MAX(tempo_conclusione) + (a.tempo_di_spedizione * interval '1 minute')) + interval '1 hour')) as time)) AS tempo FROM composto_da as c, ordine as o, area_geografica as a WHERE c.id_ordine = o.id_ordine AND o.id_area_cap = a.id_area_cap AND c.id_ordine = (SELECT MAX(id_ordine) FROM ordine WHERE id_utente = $utente) GROUP by a.id_area_cap";
			$result = pg_query($query);
			$row = pg_fetch_assoc($result);
			$tempo_previsto=$row['tempo'];
			print $tempo_previsto;
			?>
			<br />
			
			<?php
			     if($tempo_richiesto!=NULL)
					if($tempo_previsto>$tempo_richiesto)
						echo "E l'hai richiesto alle ".$tempo_richiesto." desideri confermare l'ordine?"
		
			?>
            <div class="clear"></div>
           </fieldset>
          <p>
            <input type="submit" value="Place Order &amp; Checkout" id="checkout" name="checkout" class="button" />
          </p>
          <input type="hidden" value="30" name="comment_post_ID" />
        <a href="delete_order.php" class="continueShop"> cancel </a>
        </form>
      </div>

	
    </div>
  </div>
  <div class="clear"></div>
</div>
  
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>