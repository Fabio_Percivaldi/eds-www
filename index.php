<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start(); 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
<script type="text/javascript" src="scripts/jquery-1.4.2.js"></script>
<script type="text/javascript" src="scripts/jquery.tools.min.js"></script>
<script type="text/javascript" src="scripts/dapur.js"></script>
</head>
<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
      <div id="headright" class="grid_7 prefix_5 omega">
        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente'])	)
			{
				echo "Hi, ";
				print $_SESSION['username'];
				if(isset($_SESSION['adm']) AND $_SESSION['adm']==md5($_SESSION['username']))
					print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
				else
					print "|</span> <span class=\"myAccount\"><a href=\"my_account.php\">My Account</a></span> <a href=\"logout.php\">Logout</a></h3>";
			}	
		   else
			    print "</span> <span class=\"myAccount\"><a href=\"sign_up.php\">Sign Up</a> | </span> <a href=\"login_page.php\">Login</a></h3>"; 
		
	?> 
        
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
        <li><a href="index.php" class="aActive">Home</a></li>
        <li><a href="order_page.php" >Order &amp; Delivery</a></li>
		
      </ul>
    </div>
    <div id="stickySearch" class="grid_16">
      <div class="stickyNews grid_12 alpha">
	  <?php
				$dbconn= pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw);	
				$query = "SELECT piatto.foto as foto FROM piatto, composizione WHERE piatto.id_piatto = composizione.id_piatto AND composizione.id_menu IN (SELECT extract (DOW FROM now()))";
				$result = pg_query($query);
				$count=0;
				
				?>
        <p>Piatti del giorno: <em></em> </p>
      </div>
      <div class="search grid_4 omega">
        <form action="#" method="get">
          <input type="text" value="Type your keyword" id="s" name="s" onfocus="if (this.value == 'Type your keyword') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type your keyword';}" />
        </form>
      </div>
    </div>
	
    <div class="products grid_16">
      <div class="productsWarp">	  
        <ul>
		
				
			<?php
			while ($row= pg_fetch_assoc($result))
			{
					$foto_link=$row['foto'];
					if($foto_link)
						print  "<li class><a href=\"order_page.php\"><img src=\" " . $foto_link . "\" width=\"938\" height=\"398\" /></a></li>";
					else
						print  "<li><a href=\"order_page.php\"><img src=\"images/cake1.jpg\" width=\"938\" height=\"398\" /></a></li>";	
					$count ++;
			}
				if($count<5)
				{
					while($count++<5)
						print  "<li><a href=\"order_page.php\"><img src=\"images/cake1.jpg\" width=\"938\" height=\"398\" /></a></li>";
				}
				
			?>
		
        <li class="grid_2 alpha">
        </ul>
      </div>
    </div>
    <div class="productThumb grid_10 prefix_3 suffix_3">
      <ul>
<?php
$result = pg_query($query);	
$row= pg_fetch_assoc($result);
$foto_link=$row['foto'];
$count=0;
print  " <li class=\"grid_2 alpha\"><a href=\"order_page.php\"><img src=\" " . $foto_link . "\" width=\"100\" height=\"60\" /></a></li>";
			while ($row= pg_fetch_assoc($result))
			{
					$foto_link=$row['foto'];
					if($foto_link)
						print  " <li class=\"grid_2\"><a href=\"order_page.php\"><img src=\" " . $foto_link . "\" width=\"100\" height=\"60\" /></a></li>";
					else
						print  " <li class=\"grid_2\"><a href=\"order_page.php\"><img src=\"images/cake1.jpg\" width=\"100\" height=\"60\" /></a></li>";	
					$count++;
					if($count==4) 
						break;
			}
				
			?>
			
      </ul>
    </div>
	
  </div>
  <div class="clear"></div>
</div>
<div id="fresh">
  <div class="container_16">
    <div id="freshCake" class="grid_16">
      <div class="grid_1 alpha"> <a class="prevButton">&laquo;</a></div>
      <div class="headLine grid_14">
        <h3>Fresh from the oven</h3>
      </div>
      <div class="grid_1 omega"> <a class="nextButton">&raquo;</a></div>
    </div>
    <div class="newCakes">
      <div class="scroller">
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake1.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake2.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake3.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake4.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake2.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake3.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake4.jpg" alt="" width="220" height="120" /></a></div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
<div id="richContent">
  <div class="container_16">
    <div class="popularCakes grid_4">
      <h4>Popular Cakes</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="recommended grid_4">
      <h4>Recommended</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="specialOffer grid_4">
      <h4>Special Offer</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="orderPhone grid_4">
      <h4><em>Order by Phone</em> <span>987-654-321</span></h4>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="richContent2">
  <div class="container_16">
    <div class="fromBlog grid_4">
      <h4>From the blog</h4>
      <h5>New Recipes in Our Basket</h5>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. vivamus tempor justo sit amet metus cursus consequat. Nulla viverra, felis vel accumsan fermentum... <a href="#" class="bookMan">more &raquo;</a></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>