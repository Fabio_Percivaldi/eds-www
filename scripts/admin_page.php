<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php session_start(); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.html">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente']) AND $_SESSION['adm']==md5($_SESSION['username']) )
			{
				echo "Hi, ";
				print $_SESSION['username'];
				print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
	?>

	
        <p>Subtotal: $ 00.00</p>
        <p><span class="vChart"><a href="shoppingcart.html">View Cart</a></span> <span class="cOut"><a href="checkout.html">Checkout</a></span></p>
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="shoppingcart.php" >Order &amp; Delivery</a></li>
		<li><a href="menu_print.php">Menù</a></li>
      </ul>
    </div>
    <div id="stickySearch" class="grid_16">
      <div class="stickyNews grid_12 alpha">
        <p>Valentine’s BrownieCheese Special Package. <em>Free Delivery.</em> <a href="#" class="bookMan">More &raquo;</a></p>
      </div>
      <div class="search grid_4 omega">
        <form action="#" method="get">
          <input type="text" value="Type your keyword" id="s" name="s" onfocus="if (this.value == 'Type your keyword') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Type your keyword';}" />
        </form>
      </div>
    </div>
    <div class="checkout grid_16">
      
	  <?php 
	  /*  
	  SE VOGLIAM METTERE PHP_SELF NEL FORM DI INSERISCIPIATTO
	  
        $db = pg_connect('host=localhost port=5432 dbname=Food_Express user=postgres password=q12we34R'); 

        $nome = pg_escape_string($_POST['nome_piatto']); 
        $complessita = pg_escape_string($_POST['complessita_piatto']); 
        $tempo = pg_escape_string($_POST['tempo_piatto']); 

        $query = "INSERT INTO piatto(titolo, tempo, complessita,id_categoria) VALUES( '$nome'  , $tempo  , $complessita,1 )"; 
        $result = pg_query($query); 
        if (!$result) { 
            $errormessage = pg_last_error(); 
            echo "Error with query: " . $errormessage; 
            exit(); 
        } 
        
        pg_close(); 
	
	
		*/
        ?> 
		
	<!-- INSERIMENTO PIATTO -->
      <div class="loginPage grid_8 omega">
        <h4>Inserisci piatto</h4>
        <form action="insert_piatto.php" method="post">
          <fieldset>
            <label>Nome piatto:</label>
            <input type="text" tabindex="1" size="50" value="" name="nome_piatto" class="text" />
            <br />
            <label>Complessita:</label>
            <input type="number" tabindex="2" size="22" value="" name="complessita_piatto" class="text" />
            <br />
			<label>Tempo di preparazione:</label>
            <input type="number" tabindex="3" size="22" value="" name="tempo_piatto" class="text" />
            <br />
            <div class="clear"></div>
          </fieldset>
          <p>
            <input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
          </p>
          <input type="hidden" value="30" />
        </form>
      </div>
	  <!--INSERIMENTO MENU -->
	  <div class="loginPage grid_8 omega">
		  <h4>Inserisci Menu</h4>
			  <form action="admin_page.php" method="post" onchange="this.form.submit()">
					Scegli il giorno:
					<select name="giorno_menu">
							<?php
									$db = pg_connect('host=localhost port=5432 dbname=Food_Express user=postgres password=q12we34R'); 
									$query = "SELECT giorno FROM menu"; 
									$result = pg_query($query); 
									$i=0;
									while($row= pg_fetch_assoc($result))
											print "<option value=\"".$i++."\">".$row['giorno']."</option>";
							?>
		
					</select>
			 </form>
			  
			 
			 <!-- STAMPA PIATTI DA SELEZIONARE PER AGGIUNGERE A MENU"-->
			 <?php
				if(isset($_POST['giorno_ menu'))
				{
					$giorno= pg_escape_string($_POST['giorno_ menu'));
			 ?>
			 <form action="insert_menu.php" method="post">
				  <fieldset>
					<select multiple="multiple" name="piattoSelezionato[]">
						
					<?php	
						$query = "SELECT titolo FROM piatto,composizione,menu WHERE piatto.id_piatto=composizione.id_piatto AND composizione.id_menu=menu.id_menu AND menu.giorno='".$giorno."'"; 
						$result = pg_query($query)
						$i=0;
						while($row= pg_fetch_assoc($result))
							print "<option value=\"".$i++."\">".$row['titolo']."</option>";
						
					?>	
						
						
					</select>
				  </fieldset>
				  <p>
					<input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
				  </p>
				  <input type="hidden" value="30" />
			</form>
			 <?php 
				}
			 ?>
			 
	  </div>
	
    </div>
  </div>
  <div class="clear"></div>
</div>
  <?php
			}	
		   else
			    header("location: login_page.php");
	?> 
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>