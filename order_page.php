	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start();
?>
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | ShoppingCart</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>
<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
      <div id="headright" class="grid_7 prefix_5 omega">
                <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente'])	)
			{
				echo "Hi, ";
				print $_SESSION['username'];
				if(isset($_SESSION['adm']) AND $_SESSION['adm']==md5($_SESSION['username']))
					print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				else
					print "|</span> <span class=\"myAccount\"><a href=\"my_account.php\">My Account</a></span> <a href=\"logout.php\">Logout</a></h3>";
			}	
		   else
			   header("location: login_page.php")
			   // print "</span> <span class=\"myAccount\"><a href=\"sign_up.php\">Sign Up</a> | </span> <a href=\"login_page.php\">Login</a></h3>"; 
	?> 
        
      </div>
    </div>
   <div id="mainMenu" class="grid_16">
      <ul>
        <li><a href="index.php" >Home</a></li>
        <li><a href="order_page.php" >Order &amp; Delivery</a></li>
		
      </ul>
    </div>
	
    <div class="prodNav grid_16">
      <div class="prodHeadline grid_16">
        <h3>Il Tuo Ordine</h3>
      </div>
    </div>
	<div>
	</div>
</select>
    <div class="bodyContent grid_16">
      <div class="shopCart grid_16 alpha">
        <div class="headCart grid_16 alpha">
          <div class="itemHead grid_9 alpha"> Daily Menù</div>
          <div class="priceHead grid_2"> Price</div>
          <div class="qtyHead grid_1"> Avl</div>
          <div class="subtotalHead grid_2"> Qty</div>
          <div class="remHead grid_2 omega"> Seleziona</div>
        </div>
		<?php
      if(isset($_GET['error']))
	  {
		  if($_GET['error']==1)
			print "<h1>ERRORE: quantità richiesta non disponibile.</h1>";
		  else	
			print "<h1>ERRORE: dati mancanti per l'ordine.</h1>";  
	  } 
  ?>
		<form action="insert_order.php" method="post">
          <div class="bodyCart grid_16 alpha">
            <div class="warpCart">
       <?php
			$db= pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw);
          
				$query = "SELECT P.id_piatto,titolo, qta, prezzo, descrizione, foto FROM piatto as P, composizione WHERE P.id_piatto = composizione.id_piatto AND composizione.id_menu = (SELECT extract (dow FROM now())) ORDER BY p.id_piatto;";
				$result = pg_prepare($db, "my_query", $query);
				$result = pg_execute($db, "my_query", array());
				
				
				$i=0;
                while($row= pg_fetch_assoc($result))
				{
				$foto = $row['foto']; 
				
				$id=$row['id_piatto'];
				$titolo= $row['titolo'];
				$qta= $row['qta'];
				$prezzo= $row['prezzo'];
				$desc=$row['descrizione'];
				if($qta > 0){
				print  		"<div class=\"item grid_9 alpha\">";
                print       "<p><img src=\"".$foto."\" width=\"250\" height=\"200\"  \"alt=\"\" />".$titolo."<br />";
                print  		"<span>".$desc."</span></p></div>";
				
				print       "<div class=\"price grid_2\">";
				print		"<p>" .$prezzo. "</p>";
				print		"</div>";
				print		"<div class=\"qty grid_1\">";   //Availability
				print		"<p>" .$qta. "</p>";
				print		"</div>";
				print 		"<div class=\"subtotal grid_2\">";
				print		"<input type=\"text\" size=\"1\" value=\"0\" name=\"qty[]\" />";
				print		"</div>";
				print		"<div class=\"remove grid_2 omega\">";
				print		"<input type=\"checkbox\" name=\"selected[$i]\"/>";
				print		"</div>";
				print		"<input type=\"hidden\" name=\"id[$i]\"  value=\" ".$id." \"/>";
				print		"<input type=\"hidden\" name=\"count\"  value=\" ".$i++." \"/>";
				
				print		"<input type=\"hidden\" name=\"prezzo[]\"  value=\" ".$prezzo." \"/>";
				}
			
			}
			
?>
			</div>
		</div>
          <div class="footCart grid_16 alpha">
		  <div class="checkout grid_16">
		  <div class="billInfo grid_11 alpha">
		  
		  <fieldset>
		  <label size="20">Via: </label >
            <input type="text" tabindex="4" size="30" value="" id="via" name="via" class="text" />
			<br />
			<label>Cap: </label>
			<input type="number" tabindex="5" size="5" value="" id="cap" name="cap" class="text" />
			<br />
			<label>Orario: </label>
			<input type="time"  tabindex="6" size="5" value="" id="orario" name="orario" class="text" />
			<br />
			</br>
			<label size="20">Cliccare se si desidera la fattura: </label >
            <input type="checkbox" tabindex="7" name="fattura" class="text" id="ChkBox" onclick="showMe('div1')" />
			<br />
			
			<div id="div1" class="bodyCart item grid_9 alpha" style="display:none"> 
			<label>Dominio Socitetà:</label>
			<input type="text" tabindex="8" size="20"  name="dom_società" class="text"  /></br>
			<label>Partita Iva:</label>
			<input type="text" tabindex="9" size="20"  name="part_iva" class="text" /></br>
			<label>Indirizzo Fiscale:</label>
			<input type="text" tabindex="10" size="20"  name="ind_fiscale" class="text"  /></br>
			<label>Codice Fiscale:</label>
			<input type="text" tabindex="11" size="20"  name="cod_fiscale" class="text"  />
			</div>
			
			
			
			
			
			<div class="clear"></div>
			
          </fieldset>
		  </div>
		 </div>
            
          </div>
		  
		  </br>
		 
			
			<script>
			
			
			function showMe (box) 
			{

				var chboxs = document.getElementsByName("fattura");
				var vis = "none";
				for(var i=0;i<chboxs.length;i++) 
				{ 
					if(chboxs[i].checked)
					{
						vis = "block";
						break;
					}
				}
				document.getElementById(box).style.display = vis;

			}

			
			
			</script>
			
			
			
          <div class="buttonCart grid_16 alpha">
            
            <input type="submit" value="Checkout" name="Checkout" class="checkoutCart" />
            
            <div class="clear"></div>
          </div>
		  
        </form>
      </div>
      <div id="chooseCake" class="grid_16">
        <div class="youLike grid_16">
          <h3>You might also like</h3>
        </div>
      </div>
      <div class="newCakes">
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake1.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake2.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake3.jpg" alt="" width="220" height="120" /></a></div>
        <div class="newCake"><a href="product-details.html" class="grid_4"><img src="images/freshCake4.jpg" alt="" width="220" height="120" /></a></div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>
<div id="richContent">
  <div class="container_16">
    <div class="popularCakes grid_4">
      <h4>Popular Cakes</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="recommended grid_4">
      <h4>Recommended</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="specialOffer grid_4">
      <h4>Special Offer</h4>
      <ul>
        <li><a href="#">Ultimate Choco Brownie</a></li>
        <li><a href="#">Mokakokoa Brownie</a></li>
        <li><a href="#">CoffeeBrown</a></li>
        <li><a href="#">Delicacheese</a></li>
        <li><a href="#">Berries Cheesecake</a></li>
      </ul>
    </div>
    <div class="orderPhone grid_4">
      <h4><em>Order by Phone</em> <span>987-654-321</span></h4>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="richContent2">
  <div class="container_16">
    <div class="fromBlog grid_4">
      <h4>From the blog</h4>
      <h5>New Recipes in Our Basket</h5>
      <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. vivamus tempor justo sit amet metus cursus consequat. Nulla viverra, felis vel accumsan fermentum... <a href="#" class="bookMan">more &raquo;</a></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp;amp Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>