<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente']) AND $_SESSION['adm']==md5($_SESSION['username']) )
			{
				echo "Hi, ";
				print $_SESSION['username'];
				print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
	?>

	
        <p>Subtotal: $ 00.00</p>
        <p><span class="vChart"><a href="shoppingcart.html">View Cart</a></span> <span class="cOut"><a href="checkout.html">Checkout</a></span></p>
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="shoppingcart.php" >Order &amp; Delivery</a></li>
		<li><a href="menu_print.php">Menù</a></li>
      </ul>
    </div>
    
    <div class="checkout grid_16">
      
		

	  <!--SCEGLI GIORNO MENU -->
	  
	  <div class="loginPage grid_8 omega">
		  <h4>Inserisci Menu</h4>
			  <form action="admin_menu.php" method="post" >
					Scegli il giorno:
					<select name="giorno_menu" onchange="this.form.submit()">
							<?php
									print "<option disabled selected value> -- select a menu -- </option>";
									$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw); 
									$query = "SELECT id_menu, giorno FROM menu"; 
									
									$result = pg_query($query);
									while($row= pg_fetch_assoc($result))
											print "<option value=\"".$row['id_menu']."\">".$row['giorno']."</option>";
							?>
		
					</select>
			 </form>
			 
			 <!-- STAMPA PIATTI MENU SELEZIONATO"-->
			 <?php
				if(isset($_POST['giorno_menu']))
				{
					$giorno= pg_escape_string($_POST['giorno_menu']);
				}
				else $giorno=1;
			 ?>
			 <form action="insert_menu.php" method="post">
				  <fieldset>
					<input type="hidden" name= "id_giorno" value = " <?php print $_POST['giorno_menu']; ?> " >
					<?php
						/*Versione not prepared
						$query = "SELECT giorno FROM menu WHERE id_menu=$giorno"; 
						$result = pg_query($query);
						*/
						$query = "SELECT giorno FROM menu WHERE id_menu=$1";
						$result = pg_prepare($db, "my_query", $query);
						$result = pg_execute($db, "my_query", array($giorno));	
						
						$row= pg_fetch_assoc($result);
						
						print  "Menu: ".$row['giorno'];
						print "</br>";
					?>
					<select multiple="multiple">
					
					<?php
						/*Versione not prepared
						$query = "SELECT titolo FROM piatto,composizione,menu WHERE piatto.id_piatto=composizione.id_piatto AND composizione.id_menu=menu.id_menu AND menu.id_menu=$giorno"; 
						$result = pg_query($query);
						*/
						$query = "SELECT titolo FROM piatto,composizione,menu WHERE piatto.id_piatto=composizione.id_piatto AND composizione.id_menu=menu.id_menu AND menu.id_menu=$1";
						$result = pg_prepare($db, "my_query2", $query);
						$result = pg_execute($db, "my_query2", array($giorno));	
						
						while($row= pg_fetch_assoc($result))
							print "<option value=\"".$row['titolo']."\">".$row['titolo']."</option>";
						
					?>	
						
						
					</select>
				  
				 <!-- PIATTI DISPONIBILI --> 
				  
				  </br>Piatti disponibili: </br>
				  <div class="bodyContent grid_16">
			<div class="shopCart grid_16 alpha">
					<div class="headCart grid_16 alpha">
					  <div class="itemHead grid_9 alpha"> Piatti disponibili</div>
					  <div class="priceHead grid_2"> Price</div>
					  <div class="subtotalHead grid_2"> Qty</div>
					  <div class="remHead grid_2 omega"> Seleziona</div>
					</div>
					
				<div class="bodyCart grid_16 alpha">
					<div class="warpCart">
				  
					<?php	
						
					$query = "SELECT id_piatto,titolo,descrizione FROM piatto";
					$result = pg_query($query);
				
				$i=0;
                while($row= pg_fetch_assoc($result))
				{
				$id=$row['id_piatto'];
				$titolo= $row['titolo'];
				
				$desc=$row['descrizione'];
				
				print  		"<div class=\"item grid_9 alpha\">";
                print       "<p><a href=\"#\"><img src=\"images/flickr1.jpg\" alt=\"\" /></a>".$titolo."<br />";
                print  		"<span>".$desc."</span></p></div>";
				
				print 		"<div class=\"subtotal grid_2\">";
				print		"<input type=\"text\" size=\"1\" value=\"0\" name=\"prezzo[]\" />";
				print		"</div>";
				
				print 		"<div class=\"subtotal grid_2\">";
				print		"<input type=\"text\" size=\"1\" value=\"0\" name=\"qty[]\" />";
				print		"</div>";
				print		"<div class=\"remove grid_2 omega\">";
				print		"<input type=\"checkbox\" name=\"selected[]\"/>";
				print		"</div>";
				print		"<input type=\"hidden\" name=\"count\"  value=\" ".$i++." \"/>";
				print		"<input type=\"hidden\" name=\"id[]\"  value=\" ".$id." \"/>";
				
			}
		
						
					?>	
				
				</fieldset>				
				</br>
				
					<input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
				 
				 					
						
			
			</div>
			 </div>
		
			</form>
			 </div>
			 
			
			 
	  </div>
	
    </div>
  </div>
  <div class="clear"></div>
</div>
  <?php
			}	
		   else
			    header("location: login_page.php");
	?> 
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>