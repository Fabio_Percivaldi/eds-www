<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
		<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente']) AND $_SESSION['adm']==md5($_SESSION['username']) )
			{
				echo "Hi, ";
				print $_SESSION['username'];
				print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
			
				
	?>

        
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="order_page.php" >Order &amp; Delivery</a></li>
		
      </ul>
    </div>
    
   
	
<div class="bodyContent grid_16" >
<h5>Statistiche del Giorno</h5>
      <div class="shopCart grid_16 alpha">
        <div class="headCart grid_16 alpha">
          <div class="itemHead grid_9 alpha"> Linea</div>
          <div class="priceHead grid_2"> Piatti Preparati</div>
          <div class="qtyHead grid_1"> Valor Medio piatti  </div>
          <div class="subtotalHead grid_2">Tempo Lavoro</div>
        </div>
		
		<div class="bodyCart grid_16 alpha">
            <div class="warpCart">
	<?php 
	$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw);
	/* Versione not prepared
	$id_utente = pg_escape_string($_SESSION['id_utente']);
	$query = "SELECT id_ordine, orario, costo, via, id_area_cap FROM ordine WHERE ordine.id_utente = $id_utente";
	$result = pg_query($query);
	*/
	$id_utente = $_SESSION['id_utente'];
	$query = "SELECT l.id_linea, COUNT(c.id_piatto) as piatti, COALESCE(ROUND(AVG(p.complessita),2),0) as complessita_media, COALESCE(SUM(p.tempo),0) as tempo_lavoro
				FROM linea as l
				LEFT JOIN composto_da as c
				ON l.id_linea=c.id_linea
				LEFT JOIN piatto as p
				ON c.id_piatto=p.id_piatto
				JOIN ordine as o
				ON c.id_ordine=o.id_ordine AND CURRENT_DATE=date(o.orario)
				GROUP BY l.id_linea
				ORDER BY l.id_linea";
	$result = pg_query($db, $query);
	
	while($row = pg_fetch_assoc($result)){
		
		$id_linea = pg_escape_string($row['id_linea']);
		$piatti = pg_escape_string($row['piatti']);
		$avg = pg_escape_string($row['complessita_media']);
		$tempo_lavoro = pg_escape_string($row['tempo_lavoro']);
		
				print  		"<div class=\"item grid_9 alpha\">";
                print  		"<span>".$id_linea."</span></p></div>";
				
				print       "<div class=\"price grid_2\">";
				print		"<p>" .$piatti. "</p>";
				print		"</div>";
				print		"<div class=\"qty grid_1\">";   
				print		"<p>" .$avg. "</p>";
				print		"</div>";
				print		"<div class=\"qty grid_1\">";   
				print		"<p>" .$tempo_lavoro. "</p>";
				print		"</div>";
	}
?>

<?php
			}
			else
			    header("location: login_page.php");
?>
	</div>
	</div>
	</div>
</div>
	
	
	
	
	
	
	
	
	
	
  </div>
  <div class="clear"></div>
</div>
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>