<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente'])	)
			{
				echo "Hi, ";
				print $_SESSION['username'];
				if(isset($_SESSION['adm']) AND $_SESSION['adm']==md5($_SESSION['username']))
					print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				else
					print "|</span> <span class=\"myAccount\"><a href=\"#\">My Account</a></span> <a href=\"logout.php\">Logout</a></h3>";
			}	
		   else
			    print "</span> <span class=\"myAccount\"><a href=\"sign_up.php\">Sign Up</a> | </span> <a href=\"login_page.php\">Login</a></h3>"; 
	?> 
        
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="order_page.php" >Order &amp; Delivery</a></li>
		
      </ul>
    </div>
    
    <div class="checkout grid_16">
      </br>
      <div class="loginPage grid_8 omega">
        <h4>Cambia password</h4>
        <form method="post" action="change_psw.php">
          <fieldset>
            <label>Vecchia password:</label>
            <input type="text" tabindex="3" size="22" value=""  name="old_psw" class="text" />
            <br />
            <label for="password2">Nuova Password:</label>
            <input type="password" tabindex="4" size="22" value="" name="new_psw" class="text" />
            <br />
		    <div class="clear"></div>
          </fieldset>
          <p>
            <input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
          </p>
          <input type="hidden" value="30" />
		  </form>
      </div>
    </div>
	
	<?php
      if(isset($_GET['error']))
          print "<h1>ERRORE: vecchia password errata.</h1>";
        
  ?>
	
	
<div class="bodyContent grid_16" >
<h5>Ordini</h5>
      <div class="shopCart grid_16 alpha">
        <div class="headCart grid_16 alpha">
          <div class="itemHead grid_9 alpha"> Via</div>
		  <div class="priceHead grid_2"> Orario dell'ordine</div>
          <div class="priceHead grid_2"> Orario di consegna</div>
          <div class="qtyHead grid_1"> Costo</div>
          
        </div>
		
		<div class="bodyCart grid_16 alpha">
            <div class="warpCart">
	<?php 
	$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw);
	/* Versione not prepared
	$id_utente = pg_escape_string($_SESSION['id_utente']);
	$query = "SELECT id_ordine, orario, costo, via, id_area_cap FROM ordine WHERE ordine.id_utente = $id_utente";
	$result = pg_query($query);
	*/
	$id_utente = $_SESSION['id_utente'];
	$query = "SELECT id_ordine, date_trunc('minute', orario_desiderato) as orario_consegna,  date_trunc('minute', orario + interval '1 hour') as orario_ordine, costo, via, id_area_cap FROM ordine WHERE ordine.id_utente = $1 ORDER BY id_ordine DESC";
	$result = pg_prepare($db, "my_query", $query);
	$result = pg_execute($db, "my_query", array($id_utente));
	
	while($row = pg_fetch_assoc($result)){
		
		$id_ordine = pg_escape_string($row['id_ordine']);
		$orario_consegna = pg_escape_string($row['orario_consegna']);
		$orario_ordine = pg_escape_string($row['orario_ordine']);
		$costo = pg_escape_string($row['costo']);
		$via = pg_escape_string($row['via']);
		
				print  		"<div class=\"item grid_9 alpha\">";
                print  		"<span>".$via."</span></p></div>";
				print       "<div class=\"price grid_2\">";
				print		"<p>" .$orario_ordine. "</p>";
				print		"</div>";
				print       "<div class=\"price grid_2\">";
				print		"<p>" .$orario_consegna. "</p>";
				print		"</div>";
				print		"<div class=\"qty grid_1\">";   
				print		"<p>" .$costo. "</p>";
				print		"</div>";
				
	}
?>
	</div>
	</div>
	</div>
</div>
	
	
	
	
	
	
	
	
	
	
  </div>
  <div class="clear"></div>
</div>
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>