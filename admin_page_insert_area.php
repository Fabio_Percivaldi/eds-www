<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente']) AND $_SESSION['adm']==md5($_SESSION['username']) )
			{
				echo "Hi, ";
				print $_SESSION['username'];
				print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
			
				
	?>

	
      
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="admin_page.php" >Admin area</a></li>
		<li><a href="my_account.php" >My Account</a></li>
      </ul>
    </div>

    <div class="checkout grid_16">
      
	
	</br>	
	

	
	<!-- INSERIMENTO AREA GEOGRAFICA -->
      <div class="loginPage grid_8 omega">
        <h4>Inserisci Area</h4>
        <form action="insert_area.php" method="post">
          <fieldset>
            <label>CAP:</label>
            <input type="number" tabindex="1" size="5" value="" name="cap" class="text" min="00000" max="99999" />
            <br />
			<label>Provincia:</label>
            <input type="text" tabindex="2" maxlength="2" size="2" value="" name="provincia" class="text" />
            <br />
            <label>Città:</label>
            <input type="text" tabindex="3" maxlength="20" size="20" value="" name="citta" class="text" />
            <br />
            <div class="clear"></div>
          </fieldset>
          <p>
            <input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
          </p>
          <input type="hidden" value="30" />
        </form>
      </div>
	 
	  <?php
			if(isset($_GET['errore']))
					print "<h1>ERRORE: Cap già presente.</h1>";
		?>
			  
	
	
	<!-- STAMPA AREE GEOGRAFICHE-->
	<div class="bodyContent grid_16">
      <div class="shopCart grid_16 alpha">
        <div class="headCart grid_16 alpha">
          <div class="itemHead grid_9 alpha"> CAP</div>
          <div class="priceHead grid_2"> Provincia</div>
          <div class="priceHead grid_2"> Città</div>
     
        </div>
		<div class="bodyCart grid_16 alpha">
            <div class="warpCart">
	<?php 

	$id_utente = pg_escape_string($_SESSION['id_utente']);
	$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw);
	$query = "SELECT id_area_cap, prov, citta FROM area_geografica";
	$result = pg_query($query);
	
	while($row = pg_fetch_assoc($result)){
		
		$cap = pg_escape_string($row['id_area_cap']);
		$prov = pg_escape_string($row['prov']);
		$citta = pg_escape_string($row['citta']);
		
				print  		"<div class=\"item grid_9 alpha\">";
                print  		"<span>".$cap."</span></p></div>";
				
				print       "<div class=\"price grid_2\">";
				print		"<p>" .$prov. "</p>";
				print		"</div>";
				print		"<div class=\"price grid_2\">";   
				print		"<p>" .$citta. "</p>";
				print		"</div>";
				
	}
?>
	</div>
	</div>
	</div>
	</div>
	<!-- FINE STAMPA AREE GEOGRAFICHE-->		 
	 
	
    </div>
  </div>
  <div class="clear"></div>
</div>
  <?php
			}	
		   else
			    header("location: login_page.php");
	?> 
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>