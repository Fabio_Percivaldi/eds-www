<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php 
include("connect_db.php");
session_start() 
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>DapurKue | Login</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" media="all" href="styles/960.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/reset.css" />
<link rel="stylesheet" type="text/css" media="all" href="styles/text.css" />
<link rel="stylesheet" type="text/css" media="all" href="style.css" />
<link rel="stylesheet" type="text/css" media="all" href="themes/brown/style.css" />
</head>

<body>
<div id="warp">
  <div id="main" class="container_16">
    <div id="header" class="grid_16">
      <div id="logo" class="grid_4 alpha">
        <h1><a href="index.php">DapurKue</a></h1>
        <h2>Famously Delicious</h2>
      </div>
	  
      <div id="headright" class="grid_7 prefix_5 omega">
	  
	 
	        <h3 class="login">
	    
	<?php if(isset($_SESSION['username']) AND isset($_SESSION['id_utente']) AND $_SESSION['adm']==md5($_SESSION['username']) )
			{
				echo "Hi, ";
				print $_SESSION['username'];
				print "|</span> <span class=\"myAccount\"><a href=\"admin_page.php\">Admin area</a></span> <a href=\"logout.php\">Logout</a></h3>";
				
	?>

	
        
      </div>
    </div>
    <div id="mainMenu" class="grid_16">
      <ul>
         <li><a href="index.php" >Home</a></li>
        <li><a href="admin_page.php" >Admin area</a></li>
		<li><a href="my_account.php" >My Account</a></li>
      </ul>
    </div>

    <div class="checkout grid_16">
      

	</br>
	<!-- INSERIMENTO PIATTO -->
      <div class="loginPage grid_8 omega">
        <h4>Inserisci piatto</h4>
        <form action="insert_piatto.php" method="post">
          <fieldset>
            <label>Nome piatto:</label>
            <input type="text" tabindex="1" size="22" value="" name="nome_piatto" class="text" />
            <br />
			<label>Descrizione:</label>
            <input type="text" tabindex="2" size="50" value="" name="descrizione" class="text" />
            <br />
            <label>Complessita:</label>
            <input type="number" min="1" max="5" tabindex="3" size="1" value="" name="complessita_piatto" class="text" />
            <br />
			<label>Tempo di preparazione:</label>
            <input type="number" tabindex="4" size="22" value="" name="tempo_piatto" class="text" />
            <br />
			
			<label>Link foto(max 50 char):</label>
            <input type="text" tabindex="5" size="50" value="" name="foto_link" class="text" />
            <br />
			<label>Categoria:</label>
			<select name="categoria">
							<?php
									
									$db = pg_connect("host=".myhost." dbname=".mydb." user=".myuser." password=".mypsw); 
									$query = "SELECT id_categoria,titolo FROM categoria"; 
									
									$result = pg_query($query);
									while($row= pg_fetch_assoc($result))
											print "<option>".$row['titolo']."</option>";
							?>
		
					</select>
			<br />
            <div class="clear"></div>
          </fieldset>
          <p>
            <input type="submit" value="Inserisci" tabindex="4" name="update" class="update" />
          </p>
          <input type="hidden" value="30" />
        </form>
      </div>

	
    </div>
  </div>
  <div class="clear"></div>
</div>
  <?php
			}	
		   else
			    header("location: login_page.php");
	?> 
<div id="richContent2">
  <div class="container_16">
    <div class="lastTweet grid_4">
      <h4>Latest Tweets</h4>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
      <p><a href="#">@someone</a> yes indeed this is one hell of a free css template! <a href="#">Read More</a> <span><em>15 minutes ago</em></span></p>
    </div>
    <div class="corporateInfo grid_4">
      <h4>Corporate Info</h4>
      <ul>
        <li><a href="#">Privacy Policy</a></li>
        <li><a href="#">Term &amp; Conditions</a></li>
        <li><a href="#">Franchise</a></li>
        <li><a href="#">About us</a></li>
        <li><a href="#">FAQ</a></li>
      </ul>
    </div>
    <div class="storeDelivery grid_4">
      <h4>Store &amp; Delivery</h4>
      <ul>
        <li><a href="#">Store Locator</a></li>
        <li><a href="#">Delivery Terms &amp; Pricing</a></li>
        <li><a href="#">Delivery Coverage</a></li>
        <li><a href="#">Gift Services</a></li>
        <li><a href="#">Track my order</a></li>
      </ul>
    </div>
    <div class="socialNet grid_4">
      <h4>Keep in touch</h4>
      <ul>
        <li><a href="#" class="facebook">Facebook</a></li>
        <li><a href="#" class="twitter">Twitter</a></li>
        <li><a href="#" class="feed">Feed</a></li>
      </ul>
    </div>
    <div class="clear"></div>
  </div>
</div>
<div id="footer">
  <div class="container_16">
    <div class="copyright grid_16">
      <p class="left">Copyright &copy; 2010, Your Company Here, All Rights Reserved</p>
      <p class="right">Design by <a href="http://tokokoo.com/">Tokokoo</a> &amp; <a href="http://www.instantshift.com/">instantShift</a></p>
      <div class="clear"></div>
    </div>
    <div class="clear"></div>
  </div>
</div>
</body>
</html>